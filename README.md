# Go-Worker-Pool

This project contains all the concepts and examples related to worker pool

# About worker pool
Worker pools are a model in which a fixed number of m workers (implemented in Go with goroutines) work their way through n tasks in a work queue (implemented in Go with a channel). Work stays in a queue until a worker finishes up its current task and pulls a new one off.

# Reference links
https://brandur.org/go-worker-pool#implementing

https://medium.com/@j.d.livni/write-a-go-worker-pool-in-15-minutes-c9b42f640923