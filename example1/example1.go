package main

import(
	"fmt"
	"time"
)

/*
Problem Statement:
Implement a worker pool using goroutines and channels.
These workers will receive work on the jobs channel and send the corresponding results on results. 
We’ll sleep a second per job to simulate an expensive task.
*/

// @id:- job id which to be completed
// @jobs:- channel to receive the jobs
// @results:- channel to send job status
func worker(id int , jobs <-chan int, results chan<- int  ) {
	for j := range jobs {
		fmt.Println("Worker id ",id,"started job ", j)
		time.Sleep(time.Second)
		fmt.Println("Worker id ",id,"finished job ", j)
		results <- j
	}

}

func main() {
	fmt.Println("Simple worker pool START")
	const numJobs = 5
	jobs := make(chan int, numJobs)
	results := make(chan int, numJobs)


	// Starts 3 workers, initially there are no jobs
	for w:=1; w <= 3; w++ {
		go worker(w, jobs, results)
	}

	// Here we send 5 jobs and then close that channel to indicate that’s all the work we have.
	for j:=1; j <= numJobs; j++ {
		jobs <- j
	}
	close(jobs)

	// Here we collect all the results of the work. 
	// This also ensures that the worker goroutines have finished. 
	// An alternative way to wait for multiple goroutines is to use a WaitGroup.
	for a := 1; a <= numJobs; a++ {
        <-results
	}
	
	fmt.Println("Simple worker pool END")

}